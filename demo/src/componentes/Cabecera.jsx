import React from 'react';


export default class Cabecera extends React.Component{
    render(){
        const miau = this.props.miau
        return(
            <header className="App-header">
                <h1>Hola Mundo!</h1>
                {miau}
            </header>
        )
    }
}