import React from "react";


const validate = values => {
    const errors = {}
    if (!values.nombre) {
        errors.nombre = "Campo Obligatorio"
    }
    if (!values.apellido) {
        errors.apellido = "Campo Obligatorio"
    }
    if (!values.email) {
        errors.email = "Campo Obligatorio"
    }
    return errors
}

export default class FormularioSimple extends React.Component{
    state = {
        errors: {
        
        }
    }
    
    handleChange = ({target}) => {
        const {name, value} = target
        this.setState({ [name]: value })
        
    }

    handleSubmit = (e) =>{
        e.preventDefault()
        console.log("Prevenido!")
        const {errors, ...sinErrors} = this.state
        const results = validate(sinErrors)
        this.setState({errors: results})
        if (!Object.keys(results).length) {
            // Enviar el formulario
            console.log("Formulario Enivado")
        }
    }
    render(){
        const {errors} = this.state
        return(
            <form onSubmit={this.handleSubmit}>
                <input type="text" name="nombre" onChange={this.handleChange}/>
                    {errors.nombre && errors.nombre}
                <input type="text" name="apellido" onChange={this.handleChange}/>
                    {errors.apellido && errors.apellido}    
                <input type="text" name="email" onChange={this.handleChange}/>
                    {errors.email && errors.email}
                <input type="submit" value="Aceptar" />
            
            </form>
        )
    }
}